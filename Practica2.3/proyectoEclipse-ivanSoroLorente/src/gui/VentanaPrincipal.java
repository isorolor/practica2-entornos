package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.JSpinner;
import java.awt.Toolkit;
import javax.swing.SpinnerListModel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import java.awt.Scrollbar;
import javax.swing.JSlider;

public class VentanaPrincipal extends JFrame {

	/**
	 * @author Iv�n Soro Lorente
	 * @since 09/01/2018
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField textField_3;
	private JTextField txtCmisDocumentosmisImagenes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/imagenes/icono2.png")));
		setTitle("Mi ventana v2.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 651, 643);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Mi cuenta");
		menuBar.add(mnArchivo);

		JMenuItem mntmNuevo = new JMenuItem("Iniciar sesi\u00F3n");
		mntmNuevo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/iniciar_sesion.png")));
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmNuevaCuenta = new JMenuItem("Nueva cuenta");
		mntmNuevaCuenta.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/nueva-cuenta.png")));
		mnArchivo.add(mntmNuevaCuenta);

		JMenuItem mntmAbrir = new JMenuItem("Cerrar sesi\u00F3n");
		mntmAbrir.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/cerrar.png")));
		mnArchivo.add(mntmAbrir);

		JMenuItem mntmImprimir = new JMenuItem("Salir");
		mntmImprimir.setIcon(
				new ImageIcon(VentanaPrincipal.class.getResource("/javax/swing/plaf/metal/icons/ocean/close.gif")));
		mnArchivo.add(mntmImprimir);

		JMenu mnEditar = new JMenu("Directorios");
		menuBar.add(mnEditar);

		JMenuItem mntmPrincipal = new JMenuItem("Mi directorio principal");
		mntmPrincipal.setIcon(
				new ImageIcon(VentanaPrincipal.class.getResource("/javax/swing/plaf/metal/icons/ocean/directory.gif")));
		mnEditar.add(mntmPrincipal);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Directorios ocultos");
		mntmNewMenuItem.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/candado.png")));
		mnEditar.add(mntmNewMenuItem);

		JMenu mnVer = new JMenu("Mis archivos");
		menuBar.add(mnVer);
		
		JMenuItem mntmNuevoArchivo = new JMenuItem("Nuevo archivo");
		mntmNuevoArchivo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/nuevo.png")));
		mnVer.add(mntmNuevoArchivo);
		
		JMenuItem mntmVisibilidad_1 = new JMenuItem("Visibilidad");
		mntmVisibilidad_1.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/ojo.png")));
		mnVer.add(mntmVisibilidad_1);

		JMenu mnVentana = new JMenu("Buscar");
		mnVentana.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/search-icon.png")));
		mnVer.add(mnVentana);

		JMenuItem mntmBuscarArchivo = new JMenuItem("Buscar archivo...");
		mntmBuscarArchivo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/search-icon.png")));
		mnVentana.add(mntmBuscarArchivo);

		JMenu mnConfiguracin = new JMenu("Contactos");
		menuBar.add(mnConfiguracin);
		
		JMenuItem mntmGestionarContactos = new JMenuItem("Gestionar contactos");
		mntmGestionarContactos.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/contacts.png")));
		mnConfiguracin.add(mntmGestionarContactos);

		JMenu mnAyuda = new JMenu("Enlaces");
		menuBar.add(mnAyuda);

		JMenuItem mntmVisibilidad = new JMenuItem("Visibilidad");
		mntmVisibilidad.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/ojo.png")));
		mnAyuda.add(mntmVisibilidad);

		JMenuItem mntmPrivacidad = new JMenuItem("Privacidad");
		mntmPrivacidad.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/priv.png")));
		mnAyuda.add(mntmPrivacidad);

		JMenu mnConfiguracin_1 = new JMenu("Configuraci\u00F3n");
		menuBar.add(mnConfiguracin_1);

		JMenuItem mntmNotificaciones = new JMenuItem("Notificaciones");
		mntmNotificaciones.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/icon-recurrent.png")));
		mnConfiguracin_1.add(mntmNotificaciones);

		JMenuItem mntmOpcionesGenerales = new JMenuItem("Opciones generales");
		mntmOpcionesGenerales.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/options.png")));
		mnConfiguracin_1.add(mntmOpcionesGenerales);

		JMenuItem mntmSeguridadDeLa = new JMenuItem("Seguridad de la cuenta");
		mntmSeguridadDeLa.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/keyhole-shield.png")));
		mnConfiguracin_1.add(mntmSeguridadDeLa);

		JMenuItem mntmDonar = new JMenuItem("Donar");
		mntmDonar.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/donate 1.png")));
		mnConfiguracin_1.add(mntmDonar);

		JMenu mnAyuda_1 = new JMenu("Ayuda");
		menuBar.add(mnAyuda_1);

		JMenuItem mntmMostrarAyudaContextual = new JMenuItem("Mostrar ayuda contextual");
		mnAyuda_1.add(mntmMostrarAyudaContextual);

		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnAyuda_1.add(mntmBuscar);

		JMenuItem mntmReportarError = new JMenuItem("Reportar error");
		mnAyuda_1.add(mntmReportarError);

		JMenuItem mntmActualizaciones = new JMenuItem("Actualizaciones");
		mnAyuda_1.add(mntmActualizaciones);

		JMenuItem mntmVersin = new JMenuItem("Versi\u00F3n");
		mnAyuda_1.add(mntmVersin);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBackground(Color.ORANGE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(437, 550, 89, 23);
		contentPane.add(btnNewButton);

		textField = new JTextField();
		textField.setBounds(145, 38, 127, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(42, 41, 72, 14);
		contentPane.add(lblNombre);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(338, 38, 270, 153);
		contentPane.add(scrollPane);
		
				JTextArea textArea = new JTextArea();
				scrollPane.setViewportView(textArea);

		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setBounds(88, 218, 84, 23);
		contentPane.add(rdbtnHombre);

		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBounds(205, 218, 67, 23);
		contentPane.add(rdbtnMujer);

		JLabel lblSeleccionaTuCiudad = new JLabel("Selecciona tu ciudad:");
		lblSeleccionaTuCiudad.setBounds(44, 103, 175, 14);
		contentPane.add(lblSeleccionaTuCiudad);

		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(Color.GREEN);
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Zaragoza", "Huesca", "Teruel" }));
		comboBox.setToolTipText("");
		comboBox.setEditable(true);
		comboBox.setBounds(139, 128, 133, 20);
		contentPane.add(comboBox);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBackground(Color.PINK);
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSalir.setBounds(536, 550, 89, 23);
		contentPane.add(btnSalir);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		spinner.setBounds(68, 184, 46, 20);
		contentPane.add(spinner);
		
		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setBounds(42, 66, 72, 14);
		contentPane.add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(145, 63, 127, 20);
		contentPane.add(textField_1);
		
		JLabel lblSeleccionaTuFecha = new JLabel("Selecciona tu fecha de nacimiento:");
		lblSeleccionaTuFecha.setBounds(42, 159, 209, 14);
		contentPane.add(lblSeleccionaTuFecha);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerListModel(new String[] {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}));
		spinner_1.setBounds(118, 184, 94, 20);
		contentPane.add(spinner_1);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerListModel(new String[] {"2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900"}));
		spinner_2.setBounds(219, 184, 53, 20);
		contentPane.add(spinner_2);
		
		JLabel lblIntroduceTuNombre = new JLabel("Introduce tu nombre de usuario:");
		lblIntroduceTuNombre.setBounds(42, 259, 193, 14);
		contentPane.add(lblIntroduceTuNombre);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(118, 284, 154, 20);
		contentPane.add(textField_2);
				
				JLabel lblIntroduceTuContrasea = new JLabel("Introduce tu contrase\u00F1a:");
				lblIntroduceTuContrasea.setBounds(42, 315, 193, 14);
				contentPane.add(lblIntroduceTuContrasea);
				
				passwordField = new JPasswordField();
				passwordField.setBounds(118, 340, 154, 20);
				contentPane.add(passwordField);
				
				JLabel lblRepiteLaContrasea = new JLabel("Repite la contrase\u00F1a:");
				lblRepiteLaContrasea.setBounds(42, 371, 193, 14);
				contentPane.add(lblRepiteLaContrasea);
				
				passwordField_1 = new JPasswordField();
				passwordField_1.setBounds(118, 396, 154, 20);
				contentPane.add(passwordField_1);
				
				JLabel lblFormularioDeRegistro = new JLabel("Formulario de registro de nueva cuenta");
				lblFormularioDeRegistro.setForeground(new Color(138, 43, 226));
				lblFormularioDeRegistro.setFont(new Font("Verdana", Font.BOLD, 12));
				lblFormularioDeRegistro.setBounds(10, 10, 262, 14);
				contentPane.add(lblFormularioDeRegistro);
				
				JSeparator separator = new JSeparator();
				separator.setBounds(10, 32, 262, 23);
				contentPane.add(separator);
						
						JLabel lblIntroduceTuEmail = new JLabel("Introduce tu email:");
						lblIntroduceTuEmail.setBounds(42, 436, 193, 14);
						contentPane.add(lblIntroduceTuEmail);
						
						JSeparator separator_1 = new JSeparator();
						separator_1.setBounds(10, 492, 262, 23);
						contentPane.add(separator_1);
						
						JCheckBox chckbxAceptoLasCondiciones = new JCheckBox("He le\u00EDdo y acepto las condiciones ");
						chckbxAceptoLasCondiciones.setBounds(405, 518, 230, 23);
						contentPane.add(chckbxAceptoLasCondiciones);
						
						JButton btnRestablecerCampos = new JButton("Restablecer campos");
						btnRestablecerCampos.setBackground(Color.CYAN);
						btnRestablecerCampos.setBounds(118, 518, 152, 23);
						contentPane.add(btnRestablecerCampos);
						
						textField_3 = new JTextField();
						textField_3.setColumns(10);
						textField_3.setBounds(68, 461, 204, 20);
						contentPane.add(textField_3);
						
						JLabel lblporQuQuieres = new JLabel("\u00BFPor qu\u00E9 quieres formar parte de la comunidad?");
						lblporQuQuieres.setForeground(new Color(138, 43, 226));
						lblporQuQuieres.setFont(new Font("Verdana", Font.BOLD, 12));
						lblporQuQuieres.setBounds(305, 10, 330, 14);
						contentPane.add(lblporQuQuieres);
						
						Scrollbar scrollbar = new Scrollbar();
						scrollbar.setBounds(608, 38, 17, 153);
						contentPane.add(scrollbar);
						
						JLabel lblSeleccionarUnaImagen = new JLabel("Seleccionar una imagen de perfil");
						lblSeleccionarUnaImagen.setForeground(new Color(138, 43, 226));
						lblSeleccionarUnaImagen.setFont(new Font("Verdana", Font.BOLD, 12));
						lblSeleccionarUnaImagen.setBounds(395, 221, 230, 14);
						contentPane.add(lblSeleccionarUnaImagen);
						
						JButton button = new JButton("");
						button.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/perfil.png")));
						button.setSelectedIcon(null);
						button.setBackground(Color.WHITE);
						button.setBounds(399, 284, 209, 153);
						contentPane.add(button);
						
						JButton btnSubir = new JButton("Cargar");
						btnSubir.setBackground(new Color(152, 251, 152));
						btnSubir.setBounds(519, 246, 89, 23);
						contentPane.add(btnSubir);
						
						txtCmisDocumentosmisImagenes = new JTextField();
						txtCmisDocumentosmisImagenes.setText(" Equipo\\Bibliotecas\\Im\u00E1genes");
						txtCmisDocumentosmisImagenes.setToolTipText("");
						txtCmisDocumentosmisImagenes.setColumns(10);
						txtCmisDocumentosmisImagenes.setBounds(324, 247, 187, 20);
						contentPane.add(txtCmisDocumentosmisImagenes);
						
						JSlider slider = new JSlider();
						slider.setBounds(395, 447, 219, 23);
						contentPane.add(slider);
						
						JLabel lblAjustar = new JLabel("Ajustar imagen");
						lblAjustar.setBounds(296, 447, 89, 14);
						contentPane.add(lblAjustar);
						
						JLabel label = new JLabel("50 %");
						label.setBounds(493, 479, 53, 14);
						contentPane.add(label);
						
						JLabel label_1 = new JLabel("0");
						label_1.setBounds(395, 479, 53, 14);
						contentPane.add(label_1);
						
						JLabel label_2 = new JLabel("100");
						label_2.setBounds(591, 479, 34, 14);
						contentPane.add(label_2);
						
						JLabel lblL = new JLabel("   l");
						lblL.setBounds(437, 479, 53, 14);
						contentPane.add(lblL);
						
						JLabel lblL_1 = new JLabel("      l");
						lblL_1.setBounds(536, 481, 53, 14);
						contentPane.add(lblL_1);
	}
}
