﻿using LibreriaVS_ivanSoroLorente;
using System;

namespace PruebaLibreriaVS_ivanSoroLorente
{
    public class ProbandoLibreria
    {
        public static void Main(string[] args)
        {          
            Console.WriteLine(MetodosNumericos.potencia(9, 3));

            Console.WriteLine();

            MetodosNumericos.saltoNumeros(95, 93, 4);

            Console.WriteLine();

            Console.WriteLine(MetodosNumericos.numeroBinario(123));

            Console.WriteLine();

            Console.WriteLine(MetodosNumericos.deCentigradosAKelvin(87));

            Console.WriteLine();

            MetodosNumericos.tablaMultiplicar(5);

            Console.WriteLine();

            String cadenaLeida = "Esta es una Prueba de uso de Librería";
            String palabra = "Prueba";
            String cadena = "-9898";

            Console.WriteLine(MetodosString.palabras(cadenaLeida));

            Console.WriteLine();

            MetodosString.cantidadVocales(cadenaLeida);

            MetodosString.mayusculasMinusculas(cadenaLeida);

            Console.WriteLine();

            MetodosString.palabraMasLarga(cadenaLeida, palabra);

            Console.WriteLine();

            Console.WriteLine("int con el valor correspondiente: " + MetodosString.deCadenaAEntero(cadena));

            Console.WriteLine();

            Console.WriteLine("Pulse una tecla para terminar...");
            Console.ReadKey();
        }
    }
}