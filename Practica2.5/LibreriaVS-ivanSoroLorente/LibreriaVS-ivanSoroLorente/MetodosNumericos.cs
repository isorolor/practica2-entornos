﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_ivanSoroLorente
{
    public class MetodosNumericos
    {

        // 1- Método potencia:
        // Programa que calcula la potencia de un número a partir de su base y
        // exponente

        public static int potencia(int numeroBase, int exponente)
        {

            int resultado = 1;

            if (exponente == 0)
            {

                return 1;

            }
            else if (exponente == 1)
            {

                return numeroBase;

            }
            else
            {

                for (int i = 0; i < exponente; i++)
                {

                    resultado = resultado * numeroBase;
                }

                return resultado;
            }

        }

        // 2- Método salto entre números:
        // Programa que pide un número entero inicial, uno final y un tercer número
        // que indica el salto entre ellos. El programa muestra los números entre el
        // inicial y el final aplicando el salto

        public static void saltoNumeros(int numeroInicial, int numeroFinal, int salto)
        {

            if (numeroInicial < numeroFinal)
            {

                for (int i = numeroInicial; i < numeroFinal; i = i + salto)
                {

                    Console.WriteLine(i);
                }
            }

            else
            {

                for (int i = numeroInicial; i > numeroFinal; i = i - salto)
                {

                    Console.WriteLine(i);
                }
            }
        }

        // 3- Método de decimal a binario:
        // El programa convierte a binario un número entero del 0 al 256

        public static String numeroBinario(int numeroEntero)
        {

            int cociente;
            String restos = "";

            do
            {

                cociente = numeroEntero / 2;
                cociente++;
                restos = (numeroEntero % 2) + restos;
                numeroEntero = numeroEntero / 2;

            } while (cociente != 1);

            return restos;

        }

        // 4- Método grados centígrados a grados kelvin:

        public static int deCentigradosAKelvin(int centigrados)
        {

            int kelvin = centigrados + 273;
            return kelvin;

        }

        // 5- Tabla de multiplicar
        // Introduciendo un numéro del 1 al 10, el programa mostrará su tabla de
        // multiplicar

        public static void tablaMultiplicar(int numero)
        {

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine(numero + "x" + i + "=" + numero * i);
            }
        }
    }
}