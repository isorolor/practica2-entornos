﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_ivanSoroLorente
{
    public class MetodosString
    {

        // 1- Programa que me cuenta el número de palabras que tiene una cadena de
        // texto
        // Entendemos que las palabras están separadas por un único espacio, y que
        // la frase
        // no empieza ni termina con un espacio

        public static int palabras(String cadenaLeida)
        {

            String espacio = " ";
            int contadorEspacios = 0;
            int palabras;

            for (int i = 0; i < cadenaLeida.Length; i++)
            {

                if (cadenaLeida[i] == espacio[0])
                {
                    contadorEspacios++;
                }
            }

            palabras = contadorEspacios + 1;

            return palabras;

        }

        // 2- Programa que cuenta el número de vocales de una cadena, y muestra el
        // porcentaje de cada vocal respecto del total

        public static void cantidadVocales(String cadenaLeida)
        {

            int contadorVocales = 0;
            double contadorA = 0;
            double contadorE = 0;
            double contadorI = 0;
            double contadorO = 0;
            double contadorU = 0;

            cadenaLeida = cadenaLeida.ToLower();

            for (int i = 0; i < cadenaLeida.Length; i++)
            {

                if (cadenaLeida[i] == 'a' || cadenaLeida[i] == 'e' || cadenaLeida[i] == 'i'
                        || cadenaLeida[i] == 'o' || cadenaLeida[i] == 'u')
                {
                    contadorVocales++;
                }

                if (cadenaLeida[i] == 'a')
                {
                    contadorA++;
                }
                if (cadenaLeida[i] == 'e')
                {
                    contadorE++;
                }
                if (cadenaLeida[i] == 'i')
                {
                    contadorI++;
                }
                if (cadenaLeida[i] == 'o')
                {
                    contadorO++;
                }
                if (cadenaLeida[i] == 'u')
                {
                    contadorU++;
                }
            }

            Console.WriteLine("Vocales: " + contadorVocales);
            Console.WriteLine("Longitud cadena: " + cadenaLeida.Length);
            Console.WriteLine(
                    "% de 'a' respecto al total de caracteres: " + ((contadorA * 100) / cadenaLeida.Length) + " %");
            Console.WriteLine(
                    "% de 'e' respecto al total de caracteres: " + ((contadorE * 100) / cadenaLeida.Length) + " %");
            Console.WriteLine(
                    "% de 'i' respecto al total de caracteres: " + ((contadorI * 100) / cadenaLeida.Length) + " %");
            Console.WriteLine(
                    "% de 'o' respecto al total de caracteres: " + ((contadorO * 100) / cadenaLeida.Length) + " %");
            Console.WriteLine(
                    "% de 'u' respecto al total de caracteres: " + ((contadorU * 100) / cadenaLeida.Length) + " %");

        }

        // 3- Programa que cuenta la cantidad de mayúsculas y minúsculas de una
        // cadena

        public static void mayusculasMinusculas(String cadenaLeida)
        {

            int longitudCadena;
            int contadorMayusculas = 0;
            int contadorMinusculas = 0;
            int contadorEnyeMayuscula = 0;
            int contadorEnyeMinuscula = 0;
            int sumaMayusculas = 0;
            int sumaMinusculas = 0;

            longitudCadena = cadenaLeida.Length;

            for (int i = 0; i < longitudCadena; i++)
            {

                if (cadenaLeida[i] >= 'A' && cadenaLeida[i] <= 'Z')
                {
                    contadorMayusculas++;
                }

                if (cadenaLeida[i] >= 'a' && cadenaLeida[i] <= 'z')
                {
                    contadorMinusculas++;
                }

                if (cadenaLeida[i] == 'Ñ')
                { // Ñ
                    contadorEnyeMayuscula++;
                }

                if (cadenaLeida[i] == 'ñ')
                { // ñ
                    contadorEnyeMinuscula++;
                }
            }

            sumaMayusculas = contadorMayusculas + contadorEnyeMayuscula;
            sumaMinusculas = contadorMinusculas + contadorEnyeMinuscula;

            Console.WriteLine("\nCantidad de caracteres que son letras mayúsculas: " + sumaMayusculas);
            Console.WriteLine("Cantidad de caracteres que son letras minúsculas: " + sumaMinusculas);
        }

        // 4- Programa que lee una cadena y una palabra, y confirma si la cadena 
        // leída contiene dicha palabra

        public static void palabraMasLarga(String cadena, String palabra)
        {
            cadena = cadena.ToLower();

            palabra = palabra.ToLower();

            bool resultado = cadena.Contains(palabra);

            if (resultado == true) {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
        }

        // 5- Método de cadena a entero
        // Programa que pide una cadena y en caso de quea sea un número, devuelve un
        // int con el valor correspondiente

        public static int deCadenaAEntero(String cadena)
        {

            char caracter;
            int posicion, cifra = 0, resultado = 0, resultadoNegativo = 0;

            if (esEntero(cadena) == true)
            {

                if (cadena[0] == '-')
                {

                    for (int i = 1; i < cadena.Length; i++)
                    {

                        caracter = cadena[i];
                        posicion = cadena.Length - i;

                        if (posicion == 1)
                        {

                            cifra = (caracter - 48) * posicion;

                        }
                        else
                        {

                            cifra = (int)((caracter - 48) * Math.Pow(10, posicion) / 10);
                        }

                        resultado = resultado + cifra;
                        resultadoNegativo = 0 - resultado;
                    }

                    return resultadoNegativo;

                }
                else
                {

                    for (int i = 0; i < cadena.Length; i++)
                    {

                        caracter = cadena[i];
                        posicion = cadena.Length - i;

                        if (posicion == 1)
                        {

                            cifra = (caracter - 48) * posicion;

                        }
                        else
                        {

                            cifra = (int)((caracter - 48) * Math.Pow(10, posicion) / 10);
                        }

                        resultado = resultado + cifra;
                    }

                    return resultado;
                }
            }

            return 0;

        }

        static Boolean esEntero(String cadena)
        {

            char caracter;
            int contadorCifras = 0;

            if (cadena.StartsWith("-") && cadena.Length == 1)
            {
                return false;
            }

            else if (cadena.StartsWith("-"))
            {
                for (int i = 1; i < cadena.Length; i++)
                {
                    caracter = cadena[i];

                    if (caracter >= 48 && caracter <= 57)
                    {
                        contadorCifras++;
                    }

                }
                if (contadorCifras == cadena.Length - 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else if (!cadena.StartsWith("-"))
            {
                for (int i = 0; i < cadena.Length; i++)
                {
                    caracter = cadena[i];

                    if (caracter >= 48 && caracter <= 57)
                    {
                        contadorCifras++;
                    }

                }
                if (contadorCifras == cadena.Length)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

    }
}
