package libreriaJava;

public class ClaseMates {

	// 1- M�todo potencia:
	// Programa que calcula la potencia de un n�mero a partir de su base y
	// exponente

	public static int potencia(int base, int exponente) {

		int resultado = 1;

		if (exponente == 0) {

			return 1;

		} else if (exponente == 1) {

			return base;

		} else {

			for (int i = 0; i < exponente; i++) {

				resultado = resultado * base;
			}

			return resultado;
		}

	}

	// 2- M�todo salto entre n�meros:
	// Programa que pide un n�mero entero inicial, uno final y un tercer n�mero
	// que indica el salto entre ellos. El programa muestra los n�meros entre el
	// inicial y el final aplicando el salto

	public static void saltoNumeros(int numeroInicial, int numeroFinal, int salto) {

		if (numeroInicial < numeroFinal) {

			for (int i = numeroInicial; i < numeroFinal; i = i + salto) {

				System.out.println(i);
			}
		}

		else {

			for (int i = numeroInicial; i > numeroFinal; i = i - salto) {

				System.out.println(i);
			}
		}
	}

	// 3- M�todo de decimal a binario:
	// El programa convierte a binario un n�mero entero del 0 al 256

	public static String numeroBinario(int numeroEntero) {

		int cociente;
		String restos = "";

		do {

			cociente = numeroEntero / 2;
			cociente++;
			restos = (numeroEntero % 2) + restos;
			numeroEntero = numeroEntero / 2;

		} while (cociente != 1);

		return restos;

	}

	// 4- M�todo grados cent�grados a grados kelvin:

	public static int deCentigradosAKelvin(int centigrados) {

		int kelvin = centigrados + 273;
		return kelvin;

	}

	// 5- Tabla de multiplicar
	// Introduciendo un num�ro del 1 al 10, el programa mostrar� su tabla de
	// multiplicar

	public static void tablaMultiplicar(int numero) {

		for (int i = 1; i <= 10; i++) {
			System.out.println(numero + "x" + i + "=" + numero * i);
		}
	}
}
