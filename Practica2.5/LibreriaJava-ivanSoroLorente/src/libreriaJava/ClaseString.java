package libreriaJava;

public class ClaseString {

	// 1- Programa que me cuenta el n�mero de palabras que tiene una cadena de
	// texto
	// Entendemos que las palabras est�n separadas por un �nico espacio, y que
	// la frase
	// no empieza ni termina con un espacio

	public static int palabras(String cadenaLeida) {

		String espacio = " ";
		int contadorEspacios = 0;
		int palabras;

		for (int i = 0; i < cadenaLeida.length(); i++) {

			if (cadenaLeida.charAt(i) == espacio.charAt(0)) {
				contadorEspacios++;
			}
		}

		palabras = contadorEspacios + 1;

		return palabras;

	}

	// 2- Programa que cuenta el n�mero de vocales de una cadena, y muestra el
	// porcentaje de cada vocal respecto del total

	public static void cantidadVocales(String cadenaLeida) {

		int contadorVocales = 0;
		double contadorA = 0;
		double contadorE = 0;
		double contadorI = 0;
		double contadorO = 0;
		double contadorU = 0;

		cadenaLeida = cadenaLeida.toLowerCase();

		for (int i = 0; i < cadenaLeida.length(); i++) {

			if (cadenaLeida.charAt(i) == 'a' || cadenaLeida.charAt(i) == 'e' || cadenaLeida.charAt(i) == 'i'
					|| cadenaLeida.charAt(i) == 'o' || cadenaLeida.charAt(i) == 'u') {
				contadorVocales++;
			}

			if (cadenaLeida.charAt(i) == 'a') {
				contadorA++;
			}
			if (cadenaLeida.charAt(i) == 'e') {
				contadorE++;
			}
			if (cadenaLeida.charAt(i) == 'i') {
				contadorI++;
			}
			if (cadenaLeida.charAt(i) == 'o') {
				contadorO++;
			}
			if (cadenaLeida.charAt(i) == 'u') {
				contadorU++;
			}
		}

		System.out.println("Vocales: " + contadorVocales);
		System.out.println("Longitud cadena: " + cadenaLeida.length());
		System.out.println(
				"% de 'a' respecto al total de caracteres: " + ((contadorA * 100) / cadenaLeida.length()) + " %");
		System.out.println(
				"% de 'e' respecto al total de caracteres: " + ((contadorE * 100) / cadenaLeida.length()) + " %");
		System.out.println(
				"% de 'i' respecto al total de caracteres: " + ((contadorI * 100) / cadenaLeida.length()) + " %");
		System.out.println(
				"% de 'o' respecto al total de caracteres: " + ((contadorO * 100) / cadenaLeida.length()) + " %");
		System.out.println(
				"% de 'u' respecto al total de caracteres: " + ((contadorU * 100) / cadenaLeida.length()) + " %");

	}

	// 3- Programa que cuenta la cantidad de may�sculas y min�sculas de una
	// cadena

	public static void mayusculasMinusculas(String cadenaLeida) {

		int longitudCadena;
		int contadorMayusculas = 0;
		int contadorMinusculas = 0;
		int contadorEnyeMayuscula = 0;
		int contadorEnyeMinuscula = 0;
		int sumaMayusculas = 0;
		int sumaMinusculas = 0;

		longitudCadena = cadenaLeida.length();

		for (int i = 0; i < longitudCadena; i++) {

			if (cadenaLeida.charAt(i) >= 'A' && cadenaLeida.charAt(i) <= 'Z') {
				contadorMayusculas++;
			}

			if (cadenaLeida.charAt(i) >= 'a' && cadenaLeida.charAt(i) <= 'z') {
				contadorMinusculas++;
			}

			if (cadenaLeida.charAt(i) == '�') { // �
				contadorEnyeMayuscula++;
			}

			if (cadenaLeida.charAt(i) == '�') { // �
				contadorEnyeMinuscula++;
			}
		}

		sumaMayusculas = contadorMayusculas + contadorEnyeMayuscula;
		sumaMinusculas = contadorMinusculas + contadorEnyeMinuscula;

		System.out.println("\nCantidad de caracteres que son letras may�sculas: " + sumaMayusculas);
		System.out.println("Cantidad de caracteres que son letras min�sculas: " + sumaMinusculas);
	}

	// 4- Programa que lee la palabra m�s larga de una cadena de texto

	public static void palabraMasLarga(String cadena) {

		char caracter;
		int inicioPalabra = 0;
		String palabraMayor = "", palabraEncontrada = "";

		cadena = cadena + " ";

		for (int i = 0; i < cadena.length(); i++) {
			caracter = cadena.charAt(i);

			if (caracter == ' ') {

				palabraEncontrada = cadena.substring(inicioPalabra, i);

				if (palabraEncontrada.length() > palabraMayor.length()) {
					palabraMayor = palabraEncontrada;
				}
				inicioPalabra = i + 1;
			}
		}

		System.out.println(palabraMayor);

	}

	// 5- M�todo de cadena a entero
	// Programa que pide una cadena y en caso de quea sea un n�mero, devuelve un
	// int con el valor correspondiente

	public static int deCadenaAEntero(String cadena) {

		char caracter;
		int posicion, cifra = 0, resultado = 0, resultadoNegativo = 0;

		if (esEntero(cadena) == true) {

			if (cadena.charAt(0) == '-') {

				for (int i = 1; i < cadena.length(); i++) {

					caracter = cadena.charAt(i);
					posicion = cadena.length() - i;

					if (posicion == 1) {

						cifra = (caracter - 48) * posicion;

					} else {

						cifra = (int) ((caracter - 48) * Math.pow(10, posicion) / 10);
					}

					resultado = resultado + cifra;
					resultadoNegativo = 0 - resultado;
				}

				return resultadoNegativo;

			} else {

				for (int i = 0; i < cadena.length(); i++) {

					caracter = cadena.charAt(i);
					posicion = cadena.length() - i;

					if (posicion == 1) {

						cifra = (caracter - 48) * posicion;

					} else {

						cifra = (int) ((caracter - 48) * Math.pow(10, posicion) / 10);
					}

					resultado = resultado + cifra;
				}

				return resultado;
			}
		}

		return 0;

	}

	static boolean esEntero(String cadena) {

		char caracter;
		int contadorCifras = 0;

		if (cadena.startsWith("-") && cadena.length() == 1) {
			return false;
		}

		else if (cadena.startsWith("-")) {
			for (int i = 1; i < cadena.length(); i++) {
				caracter = cadena.charAt(i);

				if (caracter >= 48 && caracter <= 57) {
					contadorCifras++;
				}

			}
			if (contadorCifras == cadena.length() - 1) {
				return true;
			} else {
				return false;
			}

		} else if (!cadena.startsWith("-")) {
			for (int i = 0; i < cadena.length(); i++) {
				caracter = cadena.charAt(i);

				if (caracter >= 48 && caracter <= 57) {
					contadorCifras++;
				}

			}
			if (contadorCifras == cadena.length()) {
				return true;
			} else {
				return false;
			}
		}

		return true;
	}

}
