package libreria;

import libreriaJava.ClaseMates;
import libreriaJava.ClaseString;

public class Pruebas_Metodos {

	public static void main(String[] args) {
		
		// Clases mates

		System.out.println(ClaseMates.potencia(3, 9));

		System.out.println();

		ClaseMates.saltoNumeros(2, 95, 3);
		
		System.out.println();

		System.out.println(ClaseMates.numeroBinario(99));

		System.out.println();

		System.out.println(ClaseMates.deCentigradosAKelvin(1210));

		System.out.println();

		ClaseMates.tablaMultiplicar(9);
		
		// Clases String
		
		String cadenaLeida = "Esta es una Prueba de uso de Librer�a";

		String cadena = "-9898";

		System.out.println(ClaseString.palabras(cadenaLeida));

		System.out.println();

		ClaseString.cantidadVocales(cadenaLeida);

		ClaseString.mayusculasMinusculas(cadenaLeida);

		System.out.println();

		ClaseString.palabraMasLarga(cadenaLeida);

		System.out.println();

		System.out.println("int con el valor correspondiente: " + ClaseString.deCadenaAEntero(cadena));

	}

}
