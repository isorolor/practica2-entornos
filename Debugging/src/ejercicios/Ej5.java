package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */

		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;

		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		/*
		 * Explicaci�n: El programa lee un n�mero entero, y desde el 1 hasta ese n�mero
		 * le�do realiza una serie de divisiones (numeroLeido/i) a trav�s de un bucle
		 * for, donde i se va incrementando en 1 en cada iteraci�n hasta llegar a
		 * igualar al n�mero le�do. Al realizarse cada divisi�n, se comprueba que el
		 * resto es 0, y si lo es, se incrementa en uno el contador de divisores. Un
		 * n�mero primo es aquel que solo tiene dos divisores: �l mismo y el 1. As� que
		 * si una vez realizadas todas las divisiones, el contador de Divisores es igual
		 * a 2, significar� que ese n�mero solo es divisible por 1 y por �l mismo, por
		 * lo que se tratar� de un n�mero primo. En el resto de los casos al final del
		 * programa se entrar� en el if y se indicar� que el n�mero no es primo.
		 */

		for (int i = 1; i <= numeroLeido; i++) {
			if (numeroLeido % i == 0) {
				cantidadDivisores++;
			}
		}

		if (cantidadDivisores > 2) {
			System.out.println("No lo es");
		} else {
			System.out.println("Si lo es");
		}

		lector.close();
	}

}
