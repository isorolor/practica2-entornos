package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instrucción y avanzar instrucción a
		 * instrucción (step into) analizando el contenido de las variables
		 */

		Scanner lector;
		int numeroLeido;
		int resultadoDivision;

		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		/*
		 * Explicaci�n y soluci�n: El error se produce en la �ltima iteraci�n del bucle,
		 * cuando se realiza la divisi�n del n�mero le�do por i = 0. El resultado de
		 * dividir un n�mero entero entre 0 es una indeterminaci�n (infinito), por eso
		 * el programa lanza esa excepci�n. Para solucionarlo bastar�a eliminar el = en
		 * la condici�n del bucle for (i > 0), as� nunca llegar�a a dividir entre 0. La
		 * �ltima divisi�n ser�a el n�mero le�do entre i = 1.
		 */

		for (int i = numeroLeido; i > 0; i--) {
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}

		lector.close();
	}

}
