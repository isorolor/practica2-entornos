package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instrucción y avanzar instrucción a
		 * instrucción (step into) analizando el contenido de las variables
		 */

		Scanner input;
		int numeroLeido;
		String cadenaLeida;

		input = new Scanner(System.in);

		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();


		System.out.println("Introduce un numero como String");
		input.nextLine();
		cadenaLeida = input.nextLine();

		/*
		 * Explicaci�n y soluci�n: Cuando en un programa se leen por teclado datos
		 * num�ricos y datos de car�cter tipo String, al introducir los datos y pulsar
		 * el intro, este se queda guardado en el buffer de entrada. Por eso cuando el
		 * programa pide que introduzcas el n�mero c�mo String salta una excepci�n y no
		 * lo permite (porque ya ha almacenado el intro despu�s de leer el n�mero). En
		 * este caso asigna una cadena vac�a y limpia el intro. La soluci�n es limpiar
		 * el buffer de entrada si se van a leer datos de tipo caracter a continuaci�n
		 * de la lectura de n�meros. Esto se realiza ejecutando la instrucci�n
		 * input.nextLine(); despu�s de la lectura de tipo num�rico.
		 */

		if (numeroLeido == Integer.parseInt(cadenaLeida)) {
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}

		input.close();

	}
}
