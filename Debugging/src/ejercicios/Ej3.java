package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instrucción y avanzar instrucción a
		 * instrucción (step into) analizando el contenido de las variables
		 */

		Scanner lector;
		char caracter = 27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;

		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();

		/*
		 * Explicaci�n y soluci�n: El programa intenta localizar la posici�n de nuestro
		 * caracter (char 27) en la cadena introducida. Como no lo localiza, muestra una
		 * excepci�n y el programa termina. Una forma de solucionarlo es a�adir la
		 * condici�n de que si no lo encuentra, no lance una excepci�n sino que muestre
		 * un mensaje. Si lo encuentra, entonces que muestre la cadena final.
		 */

		posicionCaracter = cadenaLeida.indexOf(caracter);

		if (posicionCaracter == -1) {
			System.out.println("El caracter no se encuentra en la cadena");
		} else {

			cadenaFinal = cadenaLeida.substring(0, posicionCaracter);

			System.out.println(cadenaFinal);
		}

		lector.close();
	}

}
