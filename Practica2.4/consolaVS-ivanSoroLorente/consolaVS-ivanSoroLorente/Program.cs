﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_ivanSoroLorente
{
    class Program
    {
        static void Main(string[] args)
        {

            bool salir = false;

            while (!salir)
            {

                Console.WriteLine("Selecciona una opción:");
                Console.WriteLine("1- Número mayor.......");
                Console.WriteLine("2- Raiz cuadrada......");
                Console.WriteLine("3- Número aleatorio...");
                Console.WriteLine("4- Redondeo...........");
                Console.WriteLine("5- Salir..............");
                String respuesta = Console.ReadLine();

                switch (respuesta)
                {

                    case "1":

                        Console.WriteLine();
                        Console.WriteLine("Has seleccionado la opcion 1- Número mayor");
                        Console.WriteLine();

                        Console.WriteLine("Introduce un número entero:");
                        int numero1 = int.Parse(Console.ReadLine());

                        Console.WriteLine("Introduce otro número entero:");
                        int numero2 = int.Parse(Console.ReadLine());

                        Console.WriteLine(maximo(numero1, numero2));
                        Console.WriteLine();

                        break;

                    case "2":

                        Console.WriteLine();
                        Console.WriteLine("Has seleccionado la opcion 2- Raiz cuadrada");
                        Console.WriteLine();

                        Console.WriteLine("Introduce un número:");
                        double numero = double.Parse(Console.ReadLine());

                        Console.WriteLine(raiz(numero));
                        Console.WriteLine();

                        break;

                    case "3":

                        Console.WriteLine();
                        Console.WriteLine("Has seleccionado la opcion 3- Número aleatorio");
                        Console.WriteLine();

                        Console.WriteLine("Introduce un número. El programa generará un número aleatorio entre 0 y ese");
                        int numeroLeido = int.Parse(Console.ReadLine());

                        Console.WriteLine(aleatorio(numeroLeido));
                        Console.WriteLine();

                        break;

                    case "4":

                        Console.WriteLine();
                        Console.WriteLine("Has seleccionado la opcion 4- Redondeo");
                        Console.WriteLine();

                        Console.WriteLine("Introduce un número decimal:");
                        double deci = double.Parse(Console.ReadLine());

                        Console.WriteLine(redondeo(deci));
                        Console.WriteLine();

                        break;

                    case "5":

                        Console.WriteLine();
                        salir = true;
                        Console.WriteLine("El programa ha terminado");
                        Console.WriteLine();

                        break;

                    default:

                        Console.WriteLine();
                        Console.WriteLine("Introduce una opción correcta");
                        Console.WriteLine();

                        break;

                }
            }
        }

            static int maximo(int numero1, int numero2) {

                int resultadoMaximo;

                if (numero1 > numero2)
                {
                    resultadoMaximo = numero1;
                }
                else
                {
                    resultadoMaximo = numero2;
                }

            Console.WriteLine("Número mayor:");

            return resultadoMaximo;
        }

            static double raiz(double numero) {

            double resultado = (double)(Math.Sqrt(numero));

            Console.WriteLine("La raiz cuadrada es:");

            return resultado;

        }

            static int aleatorio(int numeroLeido) {

            Random random = new Random();

            int aleatorio = random.Next(0, numeroLeido);

            Console.WriteLine("Número aleatorio:");

            return aleatorio;

        }

        static int redondeo(double deci) {

            int redondeado = (int)Math.Round(deci);

            Console.WriteLine("Número redondeado:");

            return redondeado;

        }
    }
}
