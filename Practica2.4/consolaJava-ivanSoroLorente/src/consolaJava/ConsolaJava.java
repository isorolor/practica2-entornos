package consolaJava;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		boolean salir = false;

		while (!salir) {

			System.out.println("Selecciona una opci�n:");
			System.out.println("1- Cadena a entero....");
			System.out.println("2- Valor absoluto.....");
			System.out.println("3- Rellenar array......");
			System.out.println("4- Array inverso.......");
			System.out.println("5- Salir..............");

			int opcion = lector.nextInt();

			switch (opcion) {

			case 1:

				lector.nextLine();
				System.out.println("Has seleccionado la opcion 1");
				System.out.println("Introduce una cadena. El programa comprobar� si se trata de un n�mero entero:");
				String cadena = lector.nextLine();

				System.out.println(deCadenaAEntero(cadena));
				System.out.println();
				break;

			case 2:

				System.out.println("Has seleccionado la opcion 2");
				System.out.println("Introduce un n�mero (entero o decimal). El programa mostrar� su valor absoluto:");
				double numeroLeido = lector.nextDouble();

				System.out.println(absoluto(numeroLeido));
				System.out.println();
				break;

			case 3:

				lector.nextLine();
				System.out.println("Has seleccionado la opcion 3");
				System.out.println("El programa pide n�meros para rellenar un array de 5 elementos:");

				int[] array = new int[5];

				for (int i = 0; i < array.length; i++) {
					System.out.println("Introduce un n�mero entero [" + (i + 1) + "/5]");
					array[i] = lector.nextInt();
				}

				mostrarArrayPorPantalla(array);
				System.out.println();
				break;

			case 4:
				lector.nextLine();
				System.out.println("Has seleccionado la opcion 4");
				System.out.println(
						"El programa pide n�meros para rellenar un array de 5 elementos, y lo muestra a la inversa:");

				int[] array2 = new int[5];

				for (int i = 0; i < array2.length; i++) {
					System.out.println("Introduce un n�mero entero [" + (i + 1) + "/5]");
					array2[i] = lector.nextInt();
				}

				mostrarArrayInverso(array2);
				System.out.println();
				break;

			case 5:

				salir = true;
				System.out.println("El programa ha terminado");
				break;

			default:

				System.out.println("Introduce una opci�n v�lida");
			}

		}

		lector.close();

	}

	static int deCadenaAEntero(String cadena) {

		char caracter;
		int posicion, cifra = 0, resultado = 0, resultadoNegativo = 0;

		if (esEntero(cadena) == true) {

			if (cadena.charAt(0) == '-') {

				for (int i = 1; i < cadena.length(); i++) {

					caracter = cadena.charAt(i);
					posicion = cadena.length() - i;

					if (posicion == 1) {

						cifra = (caracter - 48) * posicion;

					} else {

						cifra = (int) ((caracter - 48) * Math.pow(10, posicion) / 10);
					}

					resultado = resultado + cifra;
					resultadoNegativo = 0 - resultado;
				}
				System.out.println("Int con el valor correspondiente:");
				return resultadoNegativo;

			} else {

				for (int i = 0; i < cadena.length(); i++) {

					caracter = cadena.charAt(i);
					posicion = cadena.length() - i;

					if (posicion == 1) {

						cifra = (caracter - 48) * posicion;

					} else {

						cifra = (int) ((caracter - 48) * Math.pow(10, posicion) / 10);
					}

					resultado = resultado + cifra;
				}
				System.out.println("Int con el valor correspondiente:");
				return resultado;
			}
		}

		return 0;

	}

	static boolean esEntero(String cadena) {

		char caracter;
		int contadorCifras = 0;

		if (cadena.startsWith("-") && cadena.length() == 1) {
			return false;
		}

		else if (cadena.startsWith("-")) {
			for (int i = 1; i < cadena.length(); i++) {
				caracter = cadena.charAt(i);

				if (caracter >= 48 && caracter <= 57) {
					contadorCifras++;
				}

			}
			if (contadorCifras == cadena.length() - 1) {
				return true;
			} else {
				return false;
			}

		} else if (!cadena.startsWith("-")) {
			for (int i = 0; i < cadena.length(); i++) {
				caracter = cadena.charAt(i);

				if (caracter >= 48 && caracter <= 57) {
					contadorCifras++;
				}

			}
			if (contadorCifras == cadena.length()) {
				return true;
			} else {
				return false;
			}
		}

		return true;
	}

	public static double absoluto(double a) {

		if (a > 0) {
			return a;
		} else {
			double b = 0 - a;
			return b;
		}

	}

	static int[] mostrarArrayPorPantalla(int[] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		return array;
	}

	private static int[] mostrarArrayInverso(int[] array2) {

		for (int i = array2.length - 1; i >= 0; i--) {
			System.out.print(array2[i] + " ");
		}
		return array2;

	}
}
